﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZeroMQ;

namespace Server
{
    public partial class Form1 : Form
    {
        Thread _t;
        bool isalive = true;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        /*
        void _thread_server()
        {
            using (var responder = new ZSocket(ZSocketType.REP))
            {
                // Bind
                responder.Bind("tcp://*:5555");

                while (true)
                {
                    // Receive
                    using (ZFrame request = responder.ReceiveFrame())
                    {

                        this.Invoke(new MethodInvoker(() => {

                            lst_info.Items.Add(request.ReadString());

                        }));

                        // Do some work
                        Thread.Sleep(1);

                        // Send
                        responder.Send(new ZFrame("server"));
                    }
                }
            }
        }

    */

        void _thread_server()
        {
            using (var publisher = new ZSocket(ZSocketType.PUB))
            {
                publisher.Bind("tcp://*:5555");

                while (isalive)
                {
                    using (var updateFrame = new ZFrame("Hello from server C#"))
                    {
                        publisher.Send(updateFrame);
                    }
                    Thread.Sleep(1000);
                }

            }
        }


        private void btn_start_Click(object sender, EventArgs e)
        {
            _t = new Thread(new ThreadStart(_thread_server));
            _t.Start();
        }

        private void btn_send_Click(object sender, EventArgs e)
        {
          
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_t != null)
            {
                _t.Abort();
                _t = null;
            }
        }
    }
}
