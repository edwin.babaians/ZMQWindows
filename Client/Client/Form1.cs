﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZeroMQ;

namespace Client
{
    public partial class Form1 : Form
    {
        Thread _t;
        bool isalive = true;

        /*
        void _thread_client()
        {
            using (var requester = new ZSocket(ZSocketType.REQ))
            {
                // Connect
                requester.Connect("tcp://127.0.0.1:5555");

                for (int n = 0; n < 10; ++n)
                {
                    string requestText = "Hello";
                    

                    // Send
                    requester.Send(new ZFrame(requestText));

                    // Receive
                    using (ZFrame reply = requester.ReceiveFrame())
                    {
                        this.Invoke(new MethodInvoker(() => {

                            lst_info.Items.Add(reply.ReadString());

                        }));
                        
                    }
                }
            }
        }
        */
        void _thread_client()
        {
            using (var subscriber = new ZSocket(ZSocketType.SUB))
            {
                // Connect
                subscriber.Connect("tcp://192.168.177.145:5555");
                subscriber.Subscribe("");

                while (isalive)
                {
                    // Receive
                    using (var replyFrame = subscriber.ReceiveFrame())
                    {
                        try
                        {
                            this.Invoke(new MethodInvoker(() =>
                            {

                                lst_info.Items.Add(replyFrame.ReadString());

                            }));
                        }
                        catch
                        {

                        }

                    }
                }
            }
           
        
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            _t = new Thread(new ThreadStart(_thread_client));
            _t.Start();
        }

        private void btn_send_Click(object sender, EventArgs e)
        {
          
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            isalive = false;
          
        }
    }
}
